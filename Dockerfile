FROM node:latest as build

COPY . .
RUN yarn install
RUN yarn build
RUN yarn install --production

FROM node:alpine
COPY --from=build ./dist /dist
COPY --from=build ./node_modules /node_modules

ENV DEV_PORT=5000
ENV PROD_PORT=8080
ENV DEV_DB_USERNAME=user
ENV DEV_DB_PASSWORD=password1234
ENV DEV_DB_NAME=med
ENV DEV_DB_HOSTNAME=med_db

ENV PROD_DB_USERNAME=user
ENV PROD_DB_PASSWORD=password1234
ENV PROD_DB_NAME=med

EXPOSE 5000
WORKDIR /dist
ENTRYPOINT ["node", "app.js", "--mode=Development"]
