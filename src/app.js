import yargs from 'yargs';
import mongoose from 'mongoose';
import logger from './services/logger';

import config from './config/config';
import server from './server';

const devMode = yargs.argv.mode === 'Development';
const port = devMode ? config.development.port : config.production.port;

const connectDb = () => {
  const developmentConnection = `mongodb://${config.development.username}:${config.development.password}@${config.development.host}/${config.development.database}`;

  const productionConnection = `mongodb://${config.production.username}${config.production.password}@${config.production.host}/${config.production.database}`;

  return mongoose.connect(devMode ? developmentConnection : productionConnection, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });
};

connectDb()
  .then(() => {
    logger.info('MongoDB connected');

    if (!process.env.secret) {
      logger.warn('No secret added to start command, login will fail!');
    }

    server.listen(port, () => {
      logger.info(`REST Server started on ${port}, with development mode: ${devMode}`);
    });
  })
  .catch(e => logger.error(e));
