import * as crypto from 'crypto';

const DEFAULT_SCRYPT_KEYLEN = 64;
const DEFAULT_SALT_BYTES = 16;
const DEFAULT_SCRYPT_PARAMETERS = {
  N: 1 << 15,
  r: 8,
  p: 1,
  maxmem: 45000000,
};

export const hash = (password, keylen = DEFAULT_SCRYPT_KEYLEN) => {
  const salt = crypto.randomBytes(DEFAULT_SALT_BYTES);
  return hashWithSalt(password, salt, keylen);
};

export const compare = (plaintext, hash) => {
  const buf = Buffer.from(hash, 'base64');
  const keylen = buf.readUInt8(0) + 1;
  const salt = buf.slice(keylen);

  return hashWithSalt(plaintext, salt).then(hashedPlaintext => hashedPlaintext === hash);
};

export const hashWithSalt = (password, salt, keylen = DEFAULT_SCRYPT_KEYLEN) => {
  if (typeof salt === 'string') {
    salt = Buffer.from(salt);
  }

  const header = createHeader(keylen);
  const totalLength = 1 + keylen + salt.length;

  return scrypt(password, salt)
    .then(derivedKey => Buffer.concat([header, derivedKey, salt], totalLength))
    .then(buf => buf.toString('base64'));
};

const createHeader = keylen => {
  const buf = Buffer.allocUnsafe(1);
  buf.writeUInt8(keylen, 0);
  return buf;
};

const scrypt = (plaintext, salt, options = DEFAULT_SCRYPT_PARAMETERS) =>
  new Promise((resolve, reject) => {
    crypto.scrypt(plaintext, salt, DEFAULT_SCRYPT_KEYLEN, {}, (err, derivedKey) => {
      if (err) {
        reject(err);
      } else {
        resolve(derivedKey);
      }
    });
  });
