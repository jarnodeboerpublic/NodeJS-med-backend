import Strategy from 'passport-local';
import User from '../models/user.model';
import { compare } from '../services/hashing';

export default passport => {
  passport.use(
    new Strategy({ usernameField: 'email' }, (email, password, done) => {
      // Match user
      User.findOne({
        email: email,
      }).then(async user => {
        if (!user) {
          return done(null, false, { message: 'That email is not registered' });
        }

        const validPassword = await compare(password, user.password);
        if (validPassword) {
          return done(null, user);
        } else {
          return done(null, false, { message: 'Password incorrect' });
        }
      });
    }),
  );

  passport.serializeUser(function(user, done) {
    done(null, user);
  });

  passport.deserializeUser(function(user, done) {
    User.findById(user._id, function(err, user) {
      done(err, user);
    });
  });
};
