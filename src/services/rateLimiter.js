import RateLimit from 'express-rate-limit';

export const rateLimitConfig = {
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 10, // limit each IP to 10 requests per windowMs
  statusCode: 429, // default 429
  message: 'Too many requests, please try again later.',
};

const limiter = new RateLimit({
  ...rateLimitConfig,
});

export default limiter;
