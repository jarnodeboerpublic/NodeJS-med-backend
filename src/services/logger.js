import yargs from 'yargs';
import winston, { format } from 'winston';

const logger = winston.createLogger({
  level: 'info',
  format: format.combine(
    format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss',
    }),
    format.errors({ stack: true }),
    format.splat(),
    format.json(),
  ),
  defaultMeta: { service: 'med-service' },
  transports: [
    new winston.transports.File({ filename: './logging/error.log', level: 'error' }),
    new winston.transports.File({ filename: './logging/combined.log' }),
  ],
});

const devMode = yargs.argv.mode === 'Development';

if (devMode) {
  logger.add(
    new winston.transports.Console({
      format: winston.format.simple(),
    }),
  );
}

export default logger;
