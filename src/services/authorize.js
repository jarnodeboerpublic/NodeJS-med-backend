export const checkAuthenticated = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  }

  res.status(401);
  res.end();
};

export const checkAuthorized = requiredRoles => (req, res, next) => {
  if (requiredRoles) {
    const { role } = req.user;

    if (requiredRoles.indexOf(role) === -1) {
      res.status(403);
      res.end();
      return false;
    }
  }

  return next();
};
