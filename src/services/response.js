export const execute = (req, res, func) => {
  const { method } = req;

  const promise = Promise.resolve(typeof func === 'function' ? func() : func);

  return promise
    .then(docs => {
      if (isValidResponse(docs)) {
        res
          .status(statuscodes[method].success)
          .json(docs)
          .end();
        return docs;
      }
      if (isValidEmptyResponse(docs)) {
        res
          .status(statuscodes[method].empty)
          .json(docs)
          .end();
        return docs;
      }
      if (hasErrorResponse(docs)) {
        res
          .status(docs.status)
          .json({ errors: docs.errors })
          .end();
        return docs;
      } else {
        res.status(statuscodes[method].error).end();
        throw new Error('Not found');
      }
    })
    .catch(e => {
      if (!(e.message === 'Not found')) {
        res
          .status(statuscodes.error)
          .send(e)
          .end();
      }
      throw e;
    });
};

export const isValidResponse = resp => {
  return (
    !!resp &&
    ((Array.isArray(resp) && !!resp.length && !!resp[0]._id) ||
      (typeof resp === 'object' && !!resp._id))
  );
};

export const isValidEmptyResponse = resp => {
  return (
    (!!resp && Array.isArray(resp) && resp.length === 0) ||
    (typeof resp === 'object' && Object.entries(resp).length === 0)
  );
};

export const hasErrorResponse = resp => {
  return (
    !!resp &&
    !!resp.errors &&
    Array.isArray(resp.errors) &&
    resp.errors.length > 0 &&
    !!resp.status &&
    typeof resp.status === 'number'
  );
};

const statuscodes = {
  GET: {
    success: 200,
    error: 404,
    empty: 204,
  },
  POST: {
    success: 201,
    error: 404,
    empty: 204,
  },
  PUT: {
    success: 201,
    error: 404,
    empty: 204,
  },
  error: 500,
};

export default {
  execute,
};
