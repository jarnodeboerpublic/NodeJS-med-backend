import Order from '../models/order.model';
import userController from './user.controller';
import Product from '../models/product.model';

const getAllOrders = () =>
  Order.find()
    .populate('client')
    .populate({ path: 'products.product', model: Product })
    .exec();

const getOrders = userId =>
  Order.find({ client: userId })
    .populate('client')
    .populate({ path: 'products.product', model: Product })
    .exec();

const getOrdersByClientId = async userId => {
  const user = await userController.getUserByClientId(userId);
  const orders = await getOrders(userId);

  const childOrders = await Promise.all(user.clients.map(client => getOrders(client)));

  return orders.concat(childOrders.flat());
};

export default {
  getAllOrders,
  getOrders,
  getOrdersByClientId,
};
