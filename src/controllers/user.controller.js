import User from '../models/user.model';
import logger from '../services/logger';

const userFields = {
  id: 1,
  email: 1,
  name: 1,
  lastName: 1,
  addressInclNumber: 1,
  zipCode: 1,
  city: 1,
  role: 1,
  clients: 1,
};

const fetchUser = userData => {
  logger.info('Fetching user', userData);

  return User.findOneAndUpdate({ id: userData.id }, userData, {
    upsert: false,
    setDefaultsOnInsert: true,
    new: true,
    fields: userFields,
  }).exec();
};

const getUserByClientId = userId =>
  User.findById(userId, Object.keys(userFields).join(' '))
    .populate('clients')
    .exec();

const getUserBySessionId = req => getUserByClientId(req.user._id);

const updateUser = async req => {
  const user = await getUserBySessionId(req);
  return fetchUser({
    ...req.body,
    id: user.id,
  });
};

export default { fetchUser, getUserByClientId, getUserBySessionId, updateUser };
