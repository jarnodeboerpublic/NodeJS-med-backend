import Product from '../models/product.model';
import logger from '../services/logger';

export const fetchProduct = product => {
  logger.info('Fetching products', product);

  return Product.findOne({ id: product.id }).exec();
};
