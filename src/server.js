import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import session from 'express-session';
import passport from 'passport';
import orderRoutes from './routes/order.route';
import authRoutes from './routes/auth.route';
import userRoutes from './routes/user.route';
import localStratagy from './services/passport';

// Passport Config
localStratagy(passport);

const server = express();
server.use(
  helmet({
    hidePoweredBy: { setTo: 'PHP 7.2.0' },
  }),
);
server.use(cors({ origin: true, credentials: true }));

server.use(express.json()); // for parsing application/json
server.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencode

server.use(
  session({
    secret: process.env.secret || 'secret',
    resave: false,
    saveUninitialized: false,
    cookie: {
      secure: 'auto',
      expires: new Date(Date.now() + 3600000),
      maxAge: 3600000, // 3600000 is one hour
    },
  }),
);

server.use(passport.initialize());
server.use(passport.session());

// Enable if you're behind a reverse proxy like AWS
server.set('trust proxy', 1);

server.use('/api/order', orderRoutes);
server.use('/api/auth', authRoutes);
server.use('/api/user', userRoutes);

export default server;
