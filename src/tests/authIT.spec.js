import request from 'supertest';
import { startServer, stopServer } from './test-utils/server';
import * as userDummy from './test-utils/user.dummy';
import { createOrder } from './test-utils/order.dummy';
import User from '../models/user.model';
import { rateLimitConfig } from '../services/rateLimiter';

let server;

beforeAll(async () => {
  server = await startServer();
});

afterAll(() => {
  stopServer(server);
});

describe('Authentication', () => {
  beforeEach(async () => {
    await User.deleteMany();
  });

  describe('login validation', () => {
    it('should authenticate with valid login call', async () => {
      await userDummy.createUser();

      await request(server)
        .post('/api/auth/login')
        .send({
          email: userDummy.userAccount.email,
          password: userDummy.userAccount.password,
        })
        .then(data => {
          expect(data.headers['set-cookie'].length).toBe(1);
          expect(data.headers['set-cookie'][0]).toContain('connect.sid');
          expect(data.headers['set-cookie'][0]).toContain('Expires');
          expect(data.status).toEqual(200);
        });
    });

    it('should reject login with faulty email', async () => {
      await userDummy.createUser();

      await request(server)
        .post('/api/auth/login')
        .send({
          email: 'faulty_email@email.com',
          password: userDummy.userAccount.password,
        })
        .expect(401);
    });

    it('should reject login with faulty password', async () => {
      await userDummy.createUser();

      await request(server)
        .post('/api/auth/login')
        .send({
          email: userDummy.userAccount.email,
          password: 'faulty_password',
        })
        .expect(401);
    });

    it('should expire authentication after one hour', async () => {
      const session = await userDummy.createSession(server);

      const now = Date.now();
      const spy = jest.spyOn(Date, 'now').mockImplementation(() => new Date(now + 3600000));

      await session.get('/api/order').expect(401);
      spy.mockRestore();
    });
  });

  describe('logout', () => {
    it('should logout of the application', async () => {
      const session = await userDummy.createSession(server);

      await session.post('/api/auth/logout').then(response => {
        expect(response.status).toEqual(200);
        expect(response.text).toEqual('success');
      });
    });
  });

  describe('routes validation', () => {
    it('should get a user when logged in', async () => {
      const session = await userDummy.createSession(server);

      await session
        .get('/api/user')
        .expect(200)
        .then(response => {
          const data = response.body;
          expect(data.email).toEqual(userDummy.userAccount.email);
        });
    });

    it('should return a 401 if user is not logged in', async () => {
      await request(server)
        .get('/api/order')
        .expect(401);

      await request(server)
        .get('/api/user')
        .expect(401);
    });
  });

  describe('authorization', () => {
    it('should allow route when authenticated', async () => {
      const session = await userDummy.createAdminSession(server);
      await createOrder();

      await session
        .get('/api/order/all')
        .expect(200)
        .then(response => {
          expect(response.body.length).toBe(1);
        });
    });

    it('should return with 403 if not authorized', async () => {
      const session = await userDummy.createSession(server);
      await createOrder();

      await session.get('/api/order/all').expect(403);
    });
  });
});

describe('rate limiter', () => {
  it('should reject login after max attempts', async () => {
    await userDummy.createUser();
    let responseResult;

    for (let i = 0; i <= rateLimitConfig.max; i++) {
      await request(server)
        .post('/api/auth/login')
        .send({
          email: userDummy.userAccount.email,
          password: 'abc',
        })
        .then(response => {
          responseResult = response;
        });
    }

    expect(responseResult.status).toEqual(rateLimitConfig.statusCode);
    expect(responseResult.text).toEqual(rateLimitConfig.message);
  });
});
