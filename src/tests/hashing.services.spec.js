import { hashWithSalt, compare, hash } from '../services/hashing';

describe('test scrypt hashing', () => {
  const textPassword = 'thisIsMyPass';
  const salt = '1234';
  const testHash =
    'QLslFp9Q/6m0LtD9MWPVPfOwd/mnAfZ+M3+yLsMjiNfgTY6Od2uhAzuKR9KXkQVV2vnAFz1XUA2FZJW3UGhVDO8xMjM0';

  it('should hash password ', async () => {
    expect(await hashWithSalt(textPassword, salt)).toBe(testHash);
  });

  it('should compare textPassword with a fixed hash', async () => {
    expect(await compare(textPassword, testHash)).toBe(true);
  });

  it('should compare text password with a dynamic hash', async () => {
    const receivedHash = await hash(textPassword);

    expect(await compare(textPassword, receivedHash)).toBe(true);
  });
});
