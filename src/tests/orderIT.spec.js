import { startServer, stopServer } from './test-utils/server';
import { createAdminSession, createSession } from './test-utils/user.dummy';
import { createOrder, createOrders, getOrderData, orderConfig } from './test-utils/order.dummy';
import User from '../models/user.model';
import Order from '../models/order.model';
import orderController from '../controllers/order.controller';

let server;
const amountOfOrders = 3;

beforeAll(async () => {
  server = await startServer();
});

afterAll(() => {
  stopServer(server);
});

describe('Order routes', () => {
  beforeEach(async () => {
    await Order.deleteMany();
  });

  it('Should get an order by orderNumber', async () => {
    const order = await createOrder();
    const expectedOrder = await Order.findOne({ orderNumber: orderConfig.orderNumber });

    expect(expectedOrder._id).toEqual(order._id);
  });

  it('Should get an order by _id', async () => {
    const order = await createOrder();
    const expectedOrder = await Order.findById(order._id);

    expect(expectedOrder.orderNumber).toEqual(order.orderNumber);
  });

  describe('Order controller', () => {
    it('should get an order by client id', async () => {
      const client2 = {
        id: 2,
        email: 'test2@test.test',
        password: 'Welkom#01',
      };

      const orderData = await getOrderData();

      const ordersForClient1 = 3;
      const clientId = orderData.client._id;
      for (let i = 0; i < amountOfOrders + ordersForClient1; i++) {
        orderData.client = i < ordersForClient1 ? orderData.client : await new User(client2).save();
        await new Order(orderData).save();
      }

      const orders = await orderController.getOrdersByClientId(clientId);
      expect(orders.length).toEqual(ordersForClient1);
      expect(orders[0].client._id).toEqual(clientId);
    });

    it('Should populate the linked fields for all orders', async () => {
      await createOrders(amountOfOrders);
      const foundOrders = await orderController.getAllOrders();

      expect(foundOrders[0].client.name).toEqual('Tester');
      expect(foundOrders[0].products[0].product.name).toEqual('krukken');
    });

    it('Should populate the linked fields for a specific customer', async () => {
      const order = await createOrder();
      const foundOrders = await orderController.getOrdersByClientId(order.client._id);

      expect(foundOrders[0].client.name).toEqual('Tester');
      expect(foundOrders[0].products[0].product.name).toEqual('krukken');
    });

    it('should return orders for a subclient', async () => {
      const order = await createOrder();
      const client2Data = {
        id: '2',
        email: 'test2@test.nl',
        password: 'pw2',
        clients: [order.client._id],
      };

      const client2User = await new User(client2Data).save();

      const orders = await orderController.getOrdersByClientId(client2User._id);

      expect(orders[0].client._id).toEqual(order.client._id);
    });
  });

  describe('Order routes', () => {
    it('should return all orders for a user', async () => {
      const session = await createSession(server);
      await createOrders(amountOfOrders);

      await session
        .get('/api/order')
        .expect(200)
        .then(response => {
          const data = response.body;
          expect(data.length).toEqual(amountOfOrders);
          expect(data[0].orderNumber).toEqual(orderConfig.orderNumber);
        });
    });

    it('admin user should get all orders', async () => {
      const session = await createAdminSession(server);

      await createOrders(amountOfOrders);

      await session
        .get('/api/order/all')
        .expect(200)
        .then(response => {
          expect(response.body.length).toBe(3);
        });
    });

    it('client user should be rejected to get all orders', async () => {
      const session = await createSession(server);

      await createOrders(amountOfOrders);

      await session.get('/api/order/all').expect(403);
    });
  });
});
