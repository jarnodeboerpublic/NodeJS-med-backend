import { isValidResponse, execute } from '../services/response';

describe('Response handler', () => {
  describe('execute function should handle input', () => {
    const req = { method: 'GET' };
    const res = {
      status: jest.fn(() => res),
      send: jest.fn(() => res),
      json: jest.fn(() => res),
      end: jest.fn(),
    };
    const obj = { status: 'ok', _id: '1' };

    it('should handle an executed function', async () => {
      const func = jest.fn(() => obj);

      const received = await execute(req, res, func());

      expect(received).toBe(obj);
    });

    it('should handle an executable function', async () => {
      const func = jest.fn(() => obj);

      const received = await execute(req, res, func);

      expect(received).toBe(obj);
    });

    it('should handle a promise', async () => {
      const func = jest.fn(() => Promise.resolve(obj));

      const received = await execute(req, res, func());

      expect(received).toBe(obj);
    });

    it('should handle an executable promise', async () => {
      const func = jest.fn(() => Promise.resolve(obj));

      const received = await execute(req, res, func);

      expect(received).toBe(obj);
    });

    it('should handle an object', async () => {
      const received = await execute(req, res, obj);

      expect(received).toBe(obj);
    });

    it('should handle an error', async () => {
      const error = new Error(404);
      const func = jest.fn(() => Promise.reject(error));

      try {
        await execute(req, res, func());
      } catch (e) {
        expect(e).toBe(error);
      }
    });

    it('should handle empty response', async () => {
      const received = await execute(req, res, []);

      expect(received).toEqual([]);
    });

    it('should handle an error response', async () => {
      const error = { errors: [{}], status: 404 };
      const received = await execute(req, res, error);

      expect(received).toEqual(error);
    });

    it('should throw a 404 on unexpected empty response', async () => {
      try {
        await execute(req, res, undefined);
      } catch (e) {
        expect(e instanceof Error).toBe(true);
      }
    });
  });

  describe('isValidResponse function', () => {
    it('should validate the input', () => {
      expect(isValidResponse()).toBe(false);
      expect(isValidResponse(true)).toBe(false);
      expect(isValidResponse(1)).toBe(false);
      expect(isValidResponse('')).toBe(false);
      expect(isValidResponse('true')).toBe(false);
      expect(isValidResponse({})).toBe(false);
      expect(isValidResponse([])).toBe(false);
      expect(isValidResponse([{}])).toBe(false);

      expect(isValidResponse([{ _id: 'abc' }])).toBe(true);
      expect(isValidResponse({ _id: 'abc' })).toBe(true);
    });
  });
});
