import { startServer, stopServer } from './test-utils/server';
import { createProduct } from './test-utils/product.dummy';
import { fetchProduct } from '../controllers/product.controller';
import Product from '../models/product.model';

let server;

beforeAll(async () => {
  server = await startServer();
});

afterAll(() => {
  stopServer(server);
});

describe('Product domain', () => {
  beforeEach(async () => {
    await Product.deleteMany();
  });

  it('Should get a product by id', async () => {
    const product = await createProduct();
    const expectedProduct = await Product.findOne({ id: '1' });

    expect(expectedProduct._id).toEqual(product._id);
  });

  it('Should get a product by _id', async () => {
    const product = await createProduct();
    const expectedProduct = await Product.findById(product._id);

    expect(expectedProduct.id).toEqual('1');
  });

  describe('Product controller', () => {
    it('should get a product by id', async () => {
      const product = await createProduct();
      const expectedProduct = await fetchProduct({ id: '1' });

      expect(expectedProduct._id).toEqual(product._id);
    });
  });
});
