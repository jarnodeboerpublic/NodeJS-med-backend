import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';
import http from 'http';
import app from '../../server';
let mongoServer;

export const startServer = async done => {
  mongoServer = new MongoMemoryServer();
  const mongoUri = await mongoServer.getConnectionString();
  await mongoose.connect(mongoUri, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  const server = http.createServer(app);
  return server.listen(done);
};

export const stopServer = async server => {
  await mongoose.disconnect();
  await mongoServer.stop();
  await server.close();
};
