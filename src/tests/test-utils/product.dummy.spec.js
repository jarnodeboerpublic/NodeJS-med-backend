import { createProduct, productData } from './product.dummy';
import { startServer, stopServer } from './server';

describe('ProductDummy', () => {
  let server;

  beforeEach(async () => {
    server = await startServer();
  });

  afterEach(async () => {
    await stopServer(server);
  });

  it('should create a product', async () => {
    const product = await createProduct();

    expect(product.id).toEqual(productData.id);
    expect(product.name).toEqual(productData.name);
  });
});
