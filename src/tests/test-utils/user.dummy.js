import { hash } from '../../services/hashing';
import User from '../../models/user.model';
import request from 'supertest';

export const userAccount = {
  id: '1',
  email: 'test@test.test',
  name: 'Tester',
  password: 'Welkom#01',
};

export const adminAccount = {
  id: '2',
  email: 'admin@test.test',
  password: 'Welkom#01',
  role: 'ADMIN',
};

export const createUser = async (user = userAccount) => {
  const passwordHash = await hash(user.password);

  return new User({
    ...user,
    password: passwordHash,
  }).save();
};

export const createSession = async (server, user) => {
  user = user || userAccount;
  const registeredUser = User.find();
  if (!registeredUser.length) await createUser(user);

  const session = request.agent(server);

  await session.post('/api/auth/login').send({
    email: user.email,
    password: user.password,
  });

  return session;
};

export const createAdminUser = () => createUser(adminAccount);

export const createAdminSession = server => createSession(server, adminAccount);
