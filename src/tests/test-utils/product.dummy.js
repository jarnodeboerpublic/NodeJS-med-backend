import Product from '../../models/product.model';

export const productData = { id: '1', name: 'krukken' };

export const createProduct = () => new Product(productData).save();
