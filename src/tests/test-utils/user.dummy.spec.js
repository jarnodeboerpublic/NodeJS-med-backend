import * as userDummy from './user.dummy';
import { startServer, stopServer } from './server';

describe('UserDummy', () => {
  let server;

  beforeEach(async () => {
    server = await startServer();
  });

  afterEach(async () => {
    await stopServer(server);
  });

  it('should create a user', async () => {
    const user = await userDummy.createUser();
    const userAccount = userDummy.userAccount;

    expect(user.id).toEqual(userAccount.id);
    expect(user.email).toEqual(userAccount.email);
  });

  it('should create a session', async () => {
    const session = await userDummy.createSession(server);

    expect(typeof session.app).toBe('object');
    await session.post('/api/auth/logout').then(response => {
      expect(response.text).toEqual('success');
    });
  });

  it('should create an admin user', async () => {
    const user = await userDummy.createAdminUser();
    const userAccount = userDummy.adminAccount;

    expect(user.id).toEqual(userAccount.id);
    expect(user.email).toEqual(userAccount.email);
    expect(user.role).toEqual(userAccount.role);
  });

  it('should create an admin session', async () => {
    const session = await userDummy.createAdminSession(server);

    expect(typeof session.app).toBe('object');
    await session.post('/api/orders/all').expect(404);
  });
});
