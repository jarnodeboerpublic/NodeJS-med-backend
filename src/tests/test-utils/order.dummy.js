import moment from 'moment';
import Order from '../../models/order.model';
import User from '../../models/user.model';
import { createUser } from './user.dummy';
import { createProduct } from './product.dummy';

const startDateTime = moment().unix();
const endDateTime = moment()
  .add(2, 'hours')
  .unix();

export const orderConfig = {
  orderNumber: '123',
  client: '1',
  deliveryStreet: 'home',
  deliveryStreetNumber: '4a',
  deliveryResidence: 'Zwolle',
  postalCode: '8011PK',
  orderDate: moment().unix(),
  deliveryDateTimeWindow: startDateTime,
  deliveryDateTimeWindowEnd: endDateTime,
  status: 'INBEHANDELING',
  products: [{ product: '1', amount: '3' }],
};

export const getOrderData = async () => {
  const client = (await User.findOne()) || (await createUser());

  return {
    ...orderConfig,
    client: client,
    products: [{ product: await createProduct(), amount: '3' }],
  };
};

export const createOrder = async () => new Order(await getOrderData()).save();

export const createOrders = async (amount = 1) => {
  const orderData = await getOrderData();

  for (let i = 0; i < amount; i++) {
    await new Order(orderData).save();
  }
};
