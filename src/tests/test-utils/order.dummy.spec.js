import { orderConfig, createOrder, createOrders } from './order.dummy';
import { startServer, stopServer } from './server';
import Order from '../../models/order.model';

describe('OrderDummy', () => {
  let server;

  beforeEach(async () => {
    server = await startServer();
  });

  afterEach(async () => {
    await stopServer(server);
  });

  it('should create an order', async () => {
    const order = await createOrder();

    expect(order.orderNumber).toEqual(orderConfig.orderNumber);
    expect(order.client.id).toEqual(orderConfig.client);
    expect(order.deliveryStreet).toEqual(orderConfig.deliveryStreet);
    expect(order.deliveryStreetNumber).toEqual(orderConfig.deliveryStreetNumber);
    expect(order.deliveryResidence).toEqual(orderConfig.deliveryResidence);
    expect(order.postalCode).toEqual(orderConfig.postalCode);
    expect(order.orderDate).toEqual(orderConfig.orderDate);
    expect(order.deliveryDateTimeWindow).toEqual(orderConfig.deliveryDateTimeWindow);
    expect(order.deliveryDateTimeWindowEnd).toEqual(orderConfig.deliveryDateTimeWindowEnd);
    expect(order.status).toEqual(orderConfig.status);
    expect(order.products[0].product.id).toEqual(orderConfig.products[0].product);
    expect(order.products[0].amount).toEqual(orderConfig.products[0].amount);
  });

  it('should create multiple orders', async () => {
    const amountOfOrders = 10;
    await createOrders(amountOfOrders);

    const orders = await Order.find();

    expect(orders.length).toEqual(amountOfOrders);
  });

  it('should create multiple orders with a default of 1', async () => {
    await createOrders();

    const orders = await Order.find();

    expect(orders.length).toEqual(1);
  });
});
