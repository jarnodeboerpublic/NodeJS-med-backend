import userController from '../controllers/user.controller';
import { createSession, createUser, userAccount } from './test-utils/user.dummy';
import { startServer, stopServer } from './test-utils/server';
import User from '../models/user.model';

let server;

beforeAll(async () => {
  server = await startServer();
});

afterAll(() => {
  stopServer(server);
});

describe('User domain', () => {
  beforeEach(async () => {
    await User.deleteMany();
  });

  it('Should get a user by id', async () => {
    const user = await createUser();
    const expectedUser = await User.findOne({ id: userAccount.id });

    expect(expectedUser._id).toEqual(user._id);
  });

  it('Should get a user by _id', async () => {
    const user = await createUser();
    const expectedUser = await User.findById(user._id);

    expect(expectedUser.id).toEqual(userAccount.id);
  });

  describe('User controller', () => {
    it('should get a user by id', async () => {
      const user = await createUser();
      const expectedUser = await userController.getUserByClientId(user._id);

      expect(expectedUser._id).toEqual(user._id);
      expect(expectedUser.id).toEqual(userAccount.id);
    });

    it('should get a user by id and update', async () => {
      await createUser();
      const expectedUser = await userController.fetchUser({ id: userAccount.id, name: 'Henk' });

      expect(expectedUser.name).toEqual('Henk');
    });

    it('should save changes for existing user', async () => {
      const user = await new User({ ...userAccount }).save();
      const expectedUser = await userController.getUserByClientId(user._id);

      expect(expectedUser._id).toEqual(user._id);
      expectedUser.name = 'Harry';
      const updatedUser = await userController.fetchUser(expectedUser);

      expect(updatedUser._id).toEqual(user._id);
      expect(updatedUser.name).toEqual('Harry');
    });
  });
  describe('user routes', () => {
    it('should get a user based on session', async () => {
      const session = await createSession(server);

      await session
        .get('/api/user')
        .expect(200)
        .then(response => {
          const data = response.body;
          expect(data.email).toEqual(userAccount.email);
        });
    });

    it('should update a user based on sessionid', async () => {
      const session = await createSession(server);

      await session
        .put('/api/user')
        .send({
          name: 'Harry',
        })
        .expect(201)
        .then(response => {
          const data = response.body;
          expect(data.name).toEqual('Harry');
        });
    });
  });
});
