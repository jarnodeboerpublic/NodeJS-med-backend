import express from 'express';
import orderController from '../controllers/order.controller';
import { checkAuthenticated, checkAuthorized } from '../services/authorize';
import { execute } from '../services/response';
import { Roles } from '../config/roles';
import logger from '../services/logger';

const router = express.Router();

router
  .route('/')

  .get(checkAuthenticated, async (req, res) => {
    logger.info('Get all orders for logged in user');
    const userId = req.user._id;

    try {
      await execute(req, res, orderController.getOrdersByClientId(userId));
    } catch (e) {
      /* istanbul ignore next */
      logger.error('Orders not found');
    }
  });

router.get('/all', checkAuthenticated, checkAuthorized(Roles.ADMIN), async (req, res) => {
  logger.info('Get all orders for admin user');

  try {
    await execute(req, res, orderController.getAllOrders());
  } catch (e) {
    /* istanbul ignore next */
    logger.error('Orders not found');
  }
});

export default router;
