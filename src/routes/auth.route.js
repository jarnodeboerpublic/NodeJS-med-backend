import express from 'express';
import passport from 'passport';
import logger from '../services/logger';
import loginLimiter from '../services/rateLimiter';

const router = express.Router();

router.post('/login', loginLimiter, passport.authenticate('local'), async (req, res) => {
  logger.info(`Login request from ${req.user.email} succeeded`);

  res.send({
    role: req.user.role,
  });
});

router.post('/logout', (req, res) => {
  req.logOut();
  res.send('success');
});

export default router;
