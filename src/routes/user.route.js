import express from 'express';
import userController from '../controllers/user.controller';
import { checkAuthenticated } from '../services/authorize';
import { execute } from '../services/response';
import logger from '../services/logger';

const router = express.Router();

router
  .route('/')

  .get(checkAuthenticated, async (req, res) => {
    logger.info('Get user');

    try {
      await execute(req, res, userController.getUserBySessionId(req));
    } catch (e) {
      /* istanbul ignore next */
      logger.error('User not found', e);
    }
  })

  .put(checkAuthenticated, async (req, res) => {
    logger.info('User.route --> Updating User: ', req.body.name);

    req.body && req.body.password && delete req.body.password;
    req.body && req.body.email && delete req.body.email;

    try {
      await execute(req, res, userController.updateUser(req));
    } catch (e) {
      /* istanbul ignore next */
      logger.error('Failed to update user', e);
    }
  });

export default router;
