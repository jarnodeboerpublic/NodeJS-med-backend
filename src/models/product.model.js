import mongoose from 'mongoose';

export const productSchema = new mongoose.Schema({
  id: { type: String, default: 1 },
  name: { type: String, default: 'name' },
});

export default mongoose.model('Product', productSchema);
