import mongoose from 'mongoose';
import { Roles } from '../config/roles';

const roles = Object.values(Roles);

const Schema = mongoose.Schema;

export const userSchema = new Schema({
  id: {
    type: String,
    required: true,
  },
  name: {
    type: String,
  },
  lastName: {
    type: String,
  },
  addressInclNumber: {
    type: String,
  },
  zipCode: {
    type: String,
  },
  city: {
    type: String,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    default: 'CLIENT',
    enum: roles,
    immutable: true,
  },
  clients: [
    {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'User',
    },
  ],
});

export default mongoose.model('User', userSchema);
