import mongoose from 'mongoose';
import moment from 'moment';
import { statuses } from '../config/orderstatus';

const orderSchema = new mongoose.Schema({
  orderNumber: { type: String, required: true },
  client: { type: mongoose.SchemaTypes.ObjectId, ref: 'User', required: true },
  deliveryStreet: String,
  deliveryStreetNumber: String,
  deliveryResidence: String,
  postalCode: String,
  orderDate: { type: Number, default: moment().unix() },
  deliveryDateTimeWindow: { type: Number, default: moment().unix() },
  deliveryDateTimeWindowEnd: { type: Number, default: moment().unix() },
  status: {
    type: String,
    default: 'INBEHANDELING',
    enum: [...statuses],
  },
  products: [
    {
      product: { type: mongoose.SchemaTypes.ObjectId, ref: 'Product' },
      amount: { type: String, default: '1' },
    },
  ],
});

export default mongoose.model('Order', orderSchema);
